$(function(){
   // inicializando com a data
   var contagemRegressiva = new Date("Dec 26, 2018 00:00:00").getTime();

   //ativando a funÃ§Ã£o a cada um segundo
   var x = setInterval(function() {

   // pegando o horÃ¡rio atual
   var hoje = new Date().getTime();

   // subtraindo o tempo que resta entre a data escolhida e hoje
   var tempoRestante = contagemRegressiva - hoje;

   // contas pegas na internet para conseguir calcular os dias, horas, minutos e segundos
   var dias = Math.floor(tempoRestante / (1000 * 60 * 60 * 24));
   var horas = Math.floor((tempoRestante % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
   var minutos = Math.floor((tempoRestante % (1000 * 60 * 60)) / (1000 * 60));
   var segundos = Math.floor((tempoRestante % (1000 * 60)) / 1000);
   var miliSegundos = (((tempoRestante % (1000 * 60)) / 1000) - segundos).toFixed(2).replace(/[^\d]+/g,'').replace(0,'');

  if (segundos != 0 && segundos > -1) {

           
                // colcando no html na id "contador"
                document.getElementById('dias').innerHTML = dias;
                document.getElementById('horas').innerHTML = horas;
                document.getElementById('minutos').innerHTML = minutos;
                document.getElementById('segundos').innerHTML = segundos;
                document.getElementById('milisegundos').innerHTML = miliSegundos;
               
            }else if (dias == 0 && horas == 0 && minutos == 0 && segundos == 0 && miliSegundos == 0) {
                // colcando no html na id "contador"
                document.getElementById('dias').innerHTML  = 0;
                document.getElementById('horas').innerHTML  = 0;
                document.getElementById('minutos').innerHTML  = 0;
                document.getElementById('segundos').innerHTML = 0;
                document.getElementById('milisegundos').innerHTML  = 0;
               
                
            
            }

       }, 1);

    $.fn.isOnScreen = function(){
       var win = $(window);
       var viewport = {
           top : win.scrollTop(),
           left : win.scrollLeft()
       };

       viewport.right = viewport.left + win.width();
       viewport.bottom = viewport.top + win.height();

       var bounds = this.offset();
       bounds.right = bounds.left + this.outerWidth();
       bounds.bottom = bounds.top + this.outerHeight();

       return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
   };

   var testeMostrarVisita = false;
   $(window).scroll(function(){

       if ($('.cases').isOnScreen() == true && testeMostrarVisita == false) {

          testeMostrarVisita = true;
          setTimeout(function(){
              $(".cases  small.number").each(function() {
                  var o = $(this);
                   $({Counter: 0}).animate({
                       Counter: o.attr("data-stop")
                   },{
                       duration: 5e3,
                       easing: "swing",
                       step: function(e) {
                           o.text(Math.ceil(e))
                       }
                   })
               });
               $({Counter: 0}).animate({
                   Counter: o.attr("data-stop")
               },{
                   duration: 5e3,
                   easing: "swing",
                   step: function(e) {
                       o.text(Math.ceil(e))
                   }
               });
           }, 1);

       }
   });

   $('a.scrollTop').click(function() {
       if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
           var target = $(this.hash);
           target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
               $('html,body').animate({
                   scrollTop: target.offset().top
               }, 1000);
               return false;
           }
       }

   });



   // SCRIPTS HEIGHT 100% MODAL
   $(window).bind('scroll', function () {
      var alturaScroll = $(window).scrollTop()
      if (alturaScroll > 50) {
           $("header").addClass("topoFixed");
      }else{
           $("header").removeClass("topoFixed");
      }
   });

   
  $('.pg-inicial .quemFesRecomenda ul li').click(function(){
    let idVideo = $(this).attr('data-id');
    $(this).removeClass('ativo');
    $('.pg-inicial .quemFesRecomenda .areaaVideo iframe').removeClass('ativo');
    $('.pg-inicial .quemFesRecomenda .areaaVideo iframe#'+idVideo).addClass('ativo');
    $('.pg-inicial .quemFesRecomenda ul li').removeClass('ativo');
    $(this).addClass('ativo');
  });

  $('.pg-inicial .secaoDepoimentos .depoimentos .depoimento .btnVerMais button').click(function(e){
    $('.pg-inicial .secaoDepoimentos .popup').addClass('show');
    var dataNome = $(this).attr('data-nome');
    var dataContent = $(this).attr('data-content');
    var dataImage = $(this).attr('data-img');
    $('#nomeDept').text($(this).attr('data-nome'));
    $('#dept').append($(this).attr('data-content'));
    $('#imgDept').attr('src', dataImage);
    $('.pg-inicial .secaoDepoimentos .lente').css("visibility", "visible");
    $('body').addClass('scrollCorrecao');
  });

  $('.pg-inicial .secaoDepoimentos .popup span').click(function(){
    $('.pg-inicial .secaoDepoimentos .popup').removeClass('show');
    $('.pg-inicial .secaoDepoimentos .lente').css("visibility", "hidden");
    $('#dept').children().remove();
    $('#dept').text('');
    $('body').removeClass('scrollCorrecao');
  });

  $('.pg-inicial .secaoDepoimentos .lente').click(function(){
    $('.pg-inicial .secaoDepoimentos .popup').removeClass('show');
    $('.pg-inicial .secaoDepoimentos .lente').css("visibility", "hidden");
    $('#dept').children().remove();
    $('#dept').text('');
    $('body').removeClass('scrollCorrecao');
  });

  $("#carrosselDepoimentos").owlCarousel({
    items: 3,
    dots: !0,
    loop: 0,
    lazyLoad: !0,
    mouseDrag: true,
    touchDrag: 1,
    autoplay: true,
    autoplayTimeout: 2e3,
    autoplayHoverPause: !0,
    animateOut: "fadeOut",
    smartSpeed: 450,

            //CARROSSEL RESPONSIVO
            responsiveClass:true,
            responsive:{
              320:{
                items:1
              },

              600:{
                items:1
              },

              991:{
                items:2
              },

              1024:{
                items:3
              },
              
              1440:{
                items:3
              },
              
              1920: {
                items:3
              },

            }
          })

         //BOTÕES DO CARROSSEL DE PARCERIA
         var carrossel_depoimentosEsquerda = $("#carrosselDepoimentos").data('owlCarousel');
         $('#btnCarrosselLeft').click(function(){
           carrossel_depoimentosEsquerda.prev();
         });
         var carrossel_depoimentosDireita = $("#carrosselDepoimentos").data('owlCarousel');
         $('#btnCarrosselRight').click(function(){
           carrossel_depoimentosDireita.next();
         });

});