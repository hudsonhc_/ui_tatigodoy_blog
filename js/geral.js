$(function(){



	/*****************************************
	*           CARROSSEIS                   *
	*****************************************/

	
	//CARROSSEL DE DESTAQUE
	$("#carrosselDestaque").owlCarousel({
		items : 1,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    //responsiveClass:true,			    
        /*responsive:{
            320:{
                items:1
            },
            600:{
                items:2
            },
           
            991:{
                items:2
            },
            1024:{
                items:3
            },
            1440:{
                items:4
            },
            			            
        }*/		    		   		    
	    
	});
	//BOTÕES DO CARROSSEL ESTANTE
	var carrossel_destaque = $("#carrosselDestaque").data('owlCarousel');
	$('.carrosselDestaqueEsquerda').click(function(){ carrossel_destaque.prev(); });
	$('.carrosselDestaqueDireita').click(function(){ carrossel_destaque.next(); });

	/*****************************************
		SCRIPTS PÁGINA SOBRE
	*******************************************/
		//CARROSSEL SOBRE TATI
		$("#carrosselSobreTati").owlCarousel({
			items : 5,
	        dots: false,
	        loop: true,
	        lazyLoad: true,
	        mouseDrag:false,
	        touchDrag  : false,
	        //autoplay:true,
		    //autoplayTimeout:1500,
		    autoplayHoverPause:false,
		    //smartSpeed: 450,
		    center: true,
		    autoplay:true,
		     autoplayTimeout:31,
		     smartSpeed:7500,

		    //CARROSSEL RESPONSIVO
		    responsiveClass:true,			    
	        responsive:{
	            320:{
	                items:3
	            },
	            600:{
	                items:3
	            },
	           
	            991:{
	                items:5
	            },
	            1024:{
	                items:5
	            },
	            1440:{
	                items:5
	            },
	            			            
	        }		    		   		    
		    
		});
		
	/*****************************************
		SCRIPTS PÁGINA ESTANTE VIRTUAL
	*******************************************/
		//CARROSSEL PRINCIPAL ESTANTE VIRTUAL	
		$("#carrosselEstante").owlCarousel({
			items : 1,
			dots: false,
			loop: true,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,
			autoplay:true,
			autoplayTimeout:3000,
			autoplayHoverPause:false,
		    //smartSpeed: 450,
		    // center: true,
		    // autoplay:true,
		    //  autoplayTimeout:31,
		    //  smartSpeed:7500,

		    //CARROSSEL RESPONSIVO
		    /*responsiveClass:true,			    
	        responsive:{
	            320:{
	                items:1
	            },
	            600:{
	                items:2
	            },
	           
	            991:{
	                items:2
	            },
	            1024:{
	                items:3
	            },
	            1440:{
	                items:4
	            },
	            			            
	        }	*/	    		   		    
		    
		});
		//BOTÕES DO CARROSSEL ESTANTE
		var carrossel_estante = $("#carrosselEstante").data('owlCarousel');
		$('.esquerdaCarrosselEstante').click(function(){ carrossel_estante.prev(); });
		$('.direitaCarrosselEstante').click(function(){ carrossel_estante.next(); });

		//CARROSSEL EBOOKS EXCLUSIVOS
		$("#carrosselEbooks").owlCarousel({
			items : 7,
	        dots: false,
	        loop: false,
	        lazyLoad: true,
	        mouseDrag:true,
	        touchDrag  : true,
	        autoplay:true,
		    autoplayTimeout:3000,
		    autoplayHoverPause:false,
		    //smartSpeed: 450,
		    // center: true,
		    // autoplay:true,
		    //  autoplayTimeout:31,
		    //  smartSpeed:7500,

		    //CARROSSEL RESPONSIVO
		    responsiveClass:true,			    
	        responsive:{
	            320:{
	                items:2
	            },
	            600:{
	                items:4
	            },
	           
	            991:{
	                items:5
	            },
	            1024:{
	                items:6
	            },
	            1440:{
	                items:7
	            },
	            			            
	        }		    		   		    
		    
		});
		//BOTÕES DO CARROSSEL EBOOKS EXCLUSIVOS
		var carrossel_ebooks = $("#carrosselEbooks").data('owlCarousel');
		$('.esquerdaCarrosselEbooks').click(function(){ carrossel_ebooks.prev(); });
		$('.direitaCarrosselEbooks').click(function(){ carrossel_ebooks.next(); });

		//CARROSSEL DOWNLOADS DIVERSOS
		$("#carrosselDownloadsDiversos").owlCarousel({
			items : 7,
	        dots: false,
	        loop: false,
	        lazyLoad: true,
	        mouseDrag:true,
	        touchDrag  : true,
	        autoplay:true,
		    autoplayTimeout:3000,
		    autoplayHoverPause:false,
		    //smartSpeed: 450,
		    // center: true,
		    // autoplay:true,
		    //  autoplayTimeout:31,
		    //  smartSpeed:7500,

		    //CARROSSEL RESPONSIVO
		    responsiveClass:true,			    
	        responsive:{
	            320:{
	                items:2
	            },
	            600:{
	                items:4
	            },
	           
	            991:{
	                items:5
	            },
	            1024:{
	                items:6
	            },
	            1440:{
	                items:7
	            },
	        }		    		   		    
		    
		});
		//BOTÕES DO CARROSSEL EBOOKS EXCLUSIVOS
		var carrossel_downloads_diversos = $("#carrosselDownloadsDiversos").data('owlCarousel');
		$('.esquerdaCarrosselDownloads').click(function(){ carrossel_downloads_diversos.prev(); });
		$('.direitaCarrosselDownloads').click(function(){ carrossel_downloads_diversos.next(); });
			
		//CARROSSEL CATEGORIAS
		$("#carrosselCategorias").owlCarousel({
			items :4,
	        dots: false,
	        loop: false,
	        lazyLoad: true,
	        mouseDrag:true,
	        touchDrag  : true,
	        autoplay:true,
		    autoplayTimeout:3000,
		    autoplayHoverPause:false,
		    //smartSpeed: 450,
		    // center: true,
		    // autoplay:true,
		    //  autoplayTimeout:31,
		    //  smartSpeed:7500,

		    //CARROSSEL RESPONSIVO
		    /*responsiveClass:true,			    
	        responsive:{
	            320:{
	                items:2
	            },
	            600:{
	                items:4
	            },
	           
	            991:{
	                items:5
	            },
	            1024:{
	                items:6
	            },
	            1440:{
	                items:7
	            },
	        }*/		    		   		    
		    
		});	

		$(window).bind('scroll', function () {
	       
	       var alturaScroll = $(window).scrollTop()
	       if(alturaScroll > 300){
	       	  $(".topo").addClass('thin');
	       }
	       else{
	       	  $(".topo").removeClass('thin');
	       }
	    });

	    $(".pg-inicial .postsPaginaInicial .categoriasPosts ul li button").click(function(){
	    	$(this).next().removeClass("abrirTab");
	    	$(this).next().addClass("abrirTab");
	    });

	 	//$( window ).load(function() {
	 	//setTimeout(function(){ $(".loading").fadeOut()}, 1000);
		// });

		$(document).ready(function(){
			setTimeout(function(){ 
				$(".loading").addClass("before");
				$(".loading").addClass("green");
				$(".loading").addClass("after");
			}, 1000);
			
		})

		$(window).load(function() {

			setTimeout(function(){ 
				$(".loading").removeClass("before");
				$(".loading").removeClass("after");
				$(".loading").removeClass("green");
				$(".loading").addClass("final");
				$(".loading").addClass("pink");
			}, 1000);
			setTimeout(function(){ 
				$(".loading").fadeOut();
			}, 2000);
		})

		$('.pg-inicial .postsPaginaInicial .abrirMenuCategorias').click(function(){
			$('.pg-inicial .postsPaginaInicial .menuCategoriasMobile').addClass('abrir');
		});

		$('.pg-inicial .postsPaginaInicial .menuCategoriasMobile .fechar').click(function(){
			$('.pg-inicial .postsPaginaInicial .menuCategoriasMobile').removeClass('abrir');
		});

		var userFeed = new Instafeed({
			get: 'user',
			userId: '35591433',
			clientId: '69e4a7b845464d6298521f0b1e27bbeb',
			accessToken: '35591433.69e4a7b.c0f9af80fd354e84838ad8e19eb3c913',
			resolution: 'standard_resolution',
			template: '<a href="{{link}}" target="_blank" id="{{id}}"><div class="itemInstagram" style="background-image:url({{image}})"><small class="likeComments"><span class="likes">{{likes}}</span><span class="comments">{{comments}}</span></small></div></a>',
			sortBy: 'most-recent',
			limit: 10,
			links: false
		});
		userFeed.run();
});